package main


import (
    "fmt"
    "log"
    "os"
    "os/exec"
)

type SetWrkDirFuncs struct{}

var SetWrkDir = SetWrkDirFuncs{}


func (SetWrkDirFuncs) SetupFunc(cmd *exec.Cmd, args ...interface{}) {
    projectPath, ok := args[0].(string)
    if !ok {
        log.Fatal("SetWrkDirFuncs.SetupFunc: argument passed is not a string")
    }
    cmd.Dir = projectPath
}


func UploadToGit(root string, remoteURL string) error {
    initTag := "0.0.0"
    argList := [][]string{

        {"init"},
        {"remote", "add", "origin", remoteURL},
        {"add", "."},
        {"commit", "-m", "test", "-q"},
        // tag push
        {"tag", "-a", initTag, "-m", "version 0.0.0"},
        {"push", "-u", "origin", "master", "-q", "--follow-tags"},
    }
    for idx := 0; idx < len(argList); idx++ {
        if err := RunCommand("git", argList[idx], SetWrkDir, root); err != nil {
            return err
        }
    }
    return nil
}

type CmdFuncs interface {
    SetupFunc(*exec.Cmd, ...interface{})
}


func RunCommand(name string, args []string, funcAndArgs ...interface{}) error {
    if name == "" {
        return fmt.Errorf("command name cannot be empty")
    }
    cmd := exec.Command(name, args...)
    cmd.Env = os.Environ()
    if os.Getenv("PATH") != "" {
        cmd.Env = append(cmd.Env, fmt.Sprintf("PATH=%s:%s", os.Getenv("PATH"), "/usr/local/bin"))
    }


    if len(funcAndArgs) > 0 {
        cmdFuncs, ok := funcAndArgs[0].(CmdFuncs)
        if !ok {
            return fmt.Errorf("first interface passed should be of type CmdFuncs")
        }
        cmdFuncs.SetupFunc(cmd, funcAndArgs[1:]...)
    }
    return cmd.Run()
}

func main(){
    root:="/Users/guojunchu/go/src/gitpushtest"
    remoteUrl:="https://oauth2:t1z1y81xxzXJS87xNd-4@gitlab.com/junchuguo/maineee.git"
    err:=UploadToGit(root,remoteUrl)
    if err!=nil{
        fmt.Println(err.Error())
    }
}